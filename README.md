# Task Management System

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/car-rental/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/car-rental/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

<p>
<details>
<summary><h2>Screenshots</h2></summary>

![Index](screenshots/Index.png "Index")
![CarSearch](screenshots/CarSearch.png "CarSearch")
![CarDetails](screenshots/CarDetails.png "CarDetails")
![EditCar](screenshots/EditCar.png "EditCar")
![Comments](screenshots/Comments.png "Comments")

</details>
</p>


## Tech Stack
- Bootstrap
- ASP.NET Core
- Entity Framework

## System Design
```mermaid
graph TB
  subgraph "Car Rental"
  UI("Car Rental UI")
  UI -- invokes --> API("Car Rental API")
  API -- invokes --> DAL("Car Rental DAL")
  end

  subgraph "MSSQL"
  DB("Database")
  DAL -- queries --> DB
end
```

## Features
- [x] User
	- [x] Profile
	- [x] Sign Up/Sign In
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Location
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Comment
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Brand
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Car
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
- [x] Rent
	- [x] Add
	- [x] Edit
	- [x] Get
	- [x] Remove
	- [x] List
