﻿using DAL.CarRental.Models;

namespace DAL.CarRental.Repositories
{
	public interface IUserRepository<T> where T : class
	{
		User Login(string email, string pass);
		User CheckDublicate(string email);
	}
}
