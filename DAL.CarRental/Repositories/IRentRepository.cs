﻿using DAL.CarRental.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.CarRental.Repositories
{
    public interface IRentRepository<T> where T : class
    {
        List<Rent> SearchRent(string country, string city, string model, int amount);
        List<Rent> GetRents();
        Rent GetRentById(int it);
        void Delete(Rent rent);

    }
}
