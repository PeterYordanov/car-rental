﻿using DAL.CarRental.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.CarRental.Repositories
{
	public interface ICarRepository<T> where T : class
	{
		List<Car> GetCars();
		Car GetCarById(int id);
	}
}
