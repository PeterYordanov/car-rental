﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.CarRental.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.CarRental.Repositories
{
    public class RentRepository<T> : IRentRepository<T> where T : class
    {
        protected readonly CarContext _context;
        protected readonly DbSet<T> _table;

        public RentRepository(CarContext context)
        {
            this._context = context;
            this._table = _context.Set<T>();
        }

        public List<Rent> GetRents()
        {
            var query = from rent in _context.Rent
                        select new Rent
                        {
                            RentId = rent.RentId,
                            CarId = rent.CarId,
                            PickupLocationId = rent.PickupLocationId,
                            ReturnLocationId = rent.ReturnLocationId,
                            PickupDate = rent.PickupDate,
                            ReturnDate = rent.ReturnDate,
                            PickupLocation = rent.PickupLocation,
                            ReturnLocation = rent.ReturnLocation,
                            IsAvailable = rent.IsAvailable,
                            Model = rent.Car.Brand.Model,
                            Year = rent.Car.Brand.Year,
                            Color = rent.Car.Brand.Color,
                            CarImage = rent.Car.CarImage,
                            Amount = rent.Car.Amount,
                            CurrentLocation = rent.Car.CurrentLocation,
                            CurrentLocationId = rent.Car.CurrentLocationId,
                            Comments = rent.Comments ?? new List<Comment>(),
                            AverageRate = (rent.Comments != null) ? rent.Comments.Where(z => z.RentId == rent.RentId).Average(p => p.Rate) : 0
                        };

            var rents = query.ToList();
            return rents;
        }

        public Rent GetRentById(int id)
        {
            var query = from rent in _context.Rent
                        where rent.RentId == id
                        select new Rent
                        {
                            RentId = rent.RentId,
                            CarId = rent.CarId,
                            IsAvailable = rent.IsAvailable,
                            PickupLocationId = rent.PickupLocationId,
                            ReturnLocationId = rent.ReturnLocationId,
                            PickupDate = rent.PickupDate,
                            ReturnDate = rent.ReturnDate,
                            PickupLocation = rent.PickupLocation,
                            ReturnLocation = rent.ReturnLocation,
                            Car = rent.Car,
                            Model = rent.Car.Brand.Model,
                            Year = rent.Car.Brand.Year,
                            Color = rent.Car.Brand.Color,
                            Amount = rent.Car.Amount,
                            CarImage = rent.Car.CarImage,
                            CurrentLocation = rent.Car.CurrentLocation,
                            CurrentLocationId = rent.Car.CurrentLocationId,
                            Comments = rent.Comments.Select(c => new Comment
                            {
                                CommentId = c.CommentId,
                                Rent = c.Rent,
                                RentId = c.RentId,
                                User = c.User,
                                UserId = c.UserId,
                                CommentText = c.CommentText,
                                Rate = c.Rate
                            })
                            .ToList() ?? new List<Comment>()
                        };

            var result = query.FirstOrDefault();
            return result;
        }

        public List<Rent> SearchRent(string country, string city, string model, int amount)
        {
            var query = from rent in _context.Rent
                        select new Rent
                        {
                            RentId = rent.RentId,
                            CarId = rent.CarId,
                            PickupLocationId = rent.PickupLocationId,
                            ReturnLocationId = rent.ReturnLocationId,
                            PickupDate = rent.PickupDate,
                            IsAvailable = rent.IsAvailable,
                            ReturnDate = rent.ReturnDate,
                            PickupLocation = rent.PickupLocation,
                            ReturnLocation = rent.ReturnLocation,
                            Car = rent.Car,
                            CarImage = rent.Car.CarImage,
                            Model = rent.Car.Brand.Model,
                            Year = rent.Car.Brand.Year,
                            Color = rent.Car.Brand.Color,
                            Amount = rent.Car.Amount,
                            CurrentLocation = rent.Car.CurrentLocation,
                            CurrentLocationId = rent.Car.CurrentLocationId,
                            AverageRate = (rent.Comments.Where(z => z.RentId == rent.RentId).ToList() != null) ? rent.Comments.Where(z => z.RentId == rent.RentId).Average(p => p.Rate) : 0
                        };
            var rents = query.ToList();

            var filtered = new List<Rent>();
            if (country != null)
            {
                filtered = rents.Where(p => p.Car.CurrentLocation.Country.Contains(country)).ToList();
            }
            if (city != null)
            {
                filtered = rents.Where(p => p.Car.CurrentLocation.City.Contains(city)).ToList();
            }
            if (model != null)
            {
                filtered = rents.Where(p => p.Model.Contains(model)).ToList();
            }
            if (amount != 0)
            {
                filtered = rents.Where(p => p.Amount == amount).ToList();
            }

            return filtered;
        }

        public void Delete(Rent rent)
        {
           var comments = _context.Comment.Where(p => p.RentId == rent.RentId);
            if (comments != null)
            {
                foreach (var comment in comments)
                {
                    _context.Comment.Remove(comment);
                    _context.SaveChanges();
                }
            }
            _context.Rent.Remove(rent);
            _context.SaveChanges();
        }
    }
}
