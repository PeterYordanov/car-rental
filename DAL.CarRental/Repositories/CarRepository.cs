﻿using DAL.CarRental.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace DAL.CarRental.Repositories
{
	public class CarRepository<T> : ICarRepository<T> where T : class
	{
		protected readonly CarContext _context;
		protected readonly DbSet<T> _table;

		public CarRepository(CarContext context)
		{
			this._context = context;
			this._table = _context.Set<T>();
		}

		public List<Car> GetCars()
		{
			var query = from car in _context.Car
						select new Car
						{
							CarId = car.CarId,
							BrandId = car.BrandId,
							CurrentLocationId = car.CurrentLocationId,
							CurrentLocation = car.CurrentLocation,
							Amount = car.Amount,
							CarImage = car.CarImage,
							Brand = car.Brand,
						};
			return query.ToList();
		}

		public Car GetCarById(int id)
		{
			var query = from car in _context.Car
						where car.CarId == id
						select new Car
						{
							CarId = car.CarId,
							BrandId = car.BrandId,
							CurrentLocationId = car.CurrentLocationId,
							CurrentLocation = car.CurrentLocation,
							Amount = car.Amount,
							CarImage = car.CarImage,
							Brand = car.Brand,
						};
			return query.FirstOrDefault();
		}

	}
}
