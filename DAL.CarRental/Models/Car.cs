﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.CarRental.Models
{
    public class Car
    {
        [Key]
        public int CarId { get; set; }
        public int CurrentLocationId { get; set; }
        [ForeignKey("CurrentLocationId")]
        public virtual Location CurrentLocation { get; set; }
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }
        public int Amount { get; set; }
        [Description("Upload Image")]
        public byte[] CarImage { get; set; }
        [NotMapped]
        public virtual ICollection <Rent> Rents { get; set; }
    }
}
