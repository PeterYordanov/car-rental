﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.CarRental.Models
{
    public class Rent
    {
        [Key]
        public int RentId { get; set; }

        public DateTime? PickupDate { get; set; }

        public DateTime? ReturnDate { get; set; }

        public int PickupLocationId { get; set; }

        public int ReturnLocationId { get; set; }

        [NotMapped]
        public double AverageRate { get; set; }

        public int CarId { get; set; }

        [ForeignKey("PickupLocationId"), Column(Order = 0)]
        public virtual Location PickupLocation { get; set; }

        [ForeignKey("ReturnLocationId"), Column(Order = 1)]
        public virtual Location ReturnLocation { get; set; }

        [ForeignKey("CarId")]
        public virtual Car Car { get; set; }

        [NotMapped]
        public int Amount { get; set; }

        [NotMapped]
        public int Year { get; set; }

        public bool? IsAvailable { get; set; }

        [NotMapped]
        public string Model { get; set; }

        [NotMapped]
        public string Color { get; set; }

        [NotMapped]
        public byte[] CarImage { get; set; }

        [NotMapped]
        public  string Image { get; set; }

        [NotMapped]
        public int CurrentLocationId { get; set; }

        [NotMapped]
        public virtual Location CurrentLocation { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
