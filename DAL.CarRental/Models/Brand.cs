﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.CarRental.Models
{
    public class Brand
    {
        [Key]
        public int BrandId { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
    }
}