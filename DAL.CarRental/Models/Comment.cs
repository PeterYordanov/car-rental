﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.CarRental.Models
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        public string CommentText  { get; set; }
        public int Rate { get; set; }
        public int RentId { get; set; }
        [ForeignKey("RentId")]
        public virtual Rent Rent { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
