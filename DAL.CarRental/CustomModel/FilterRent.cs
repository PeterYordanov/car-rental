﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.CarRental.CustomModel
{
    public class FilterRent
    {
        [FromQuery(Name = "country")]
        public string Country { get; set; }

        [FromQuery(Name = "city")]
        public string City { get; set; }

        [FromQuery(Name = "amount")]
        public int Amount { get; set; }

        [FromQuery(Name = "model")]
        public string Model { get; set; }
    }
}
