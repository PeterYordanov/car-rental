﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Web.CarRental.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        [Display(Name = "Comment")]
        public string CommentText { get; set; }
        public int Rate { get; set; }
        public int RentId { get; set; }
        [ForeignKey("RentId")]
        public virtual RentViewModel Rent { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserViewModel User { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
        public virtual ICollection<RentViewModel> Rents { get; set; }
    }
}
