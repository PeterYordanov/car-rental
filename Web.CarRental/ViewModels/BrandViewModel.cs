﻿namespace Web.CarRental.ViewModels
{
    public class BrandViewModel
    {
        public int BrandId { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
    }
}
