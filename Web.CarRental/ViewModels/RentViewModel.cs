﻿using DAL.CarRental.CustomModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.CarRental.ViewModels
{
    public class RentViewModel
    {
        public int RentId { get; set; }

        [Display(Name = "Pickup date")]
        [DataType(DataType.Date)]
        public DateTime PickupDate { get; set; }

        [Display(Name = "Return date")]
        [DataType(DataType.Date)]
        public DateTime ReturnDate { get; set; }

        public int PickupLocationId { get; set; }

        [Display(Name = "Reviews:")]
        [NotMapped]
        public double AverageRate { get; set; }

        public int ReturnLocationId { get; set; }

        public int CarId { get; set; }

        [Display(Name = "Pickup Location")]
        [ForeignKey("PickupLocationId"), Column(Order = 0)]
        public virtual LocationViewModel PickupLocation { get; set; }

        [Display(Name = "Return Location")]
        [ForeignKey("ReturnLocationId"), Column(Order = 1)]
        public virtual LocationViewModel ReturnLocation { get; set; }

        [ForeignKey("CarId")]
        public virtual CarViewModel Car { get; set; }

        public bool? IsAvailable { get; set; }

        public int Amount { get; set; }

        public int Year { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public virtual byte[] CarImage { get; set; }
        public virtual string Image { get; set; }
        public int CurrentLocationId { get; set; }
        public virtual LocationViewModel CurrentLocation { get; set; }
        public virtual FilterRent FilterRent { get; set; }
        public virtual ICollection<CommentViewModel> Comments { get; set; }
        public virtual ICollection<CarViewModel> Cars { get; set; }
        public virtual ICollection<LocationViewModel> PickupLocations { get; set; }
        public virtual ICollection<LocationViewModel> ReturnLocations { get; set; }
    }
}