﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Web.CarRental.ViewModels
{
    public class CarViewModel
    {
        public int CarId { get; set; }
        public int CurrentLocationId { get; set; }
        [ForeignKey("CurrentLocationId")]
        public virtual LocationViewModel CurrentLocation { get; set; }
        public virtual ICollection<LocationViewModel> CurrentLocations { get; set; }
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public virtual BrandViewModel Brand { get; set; }
        public virtual ICollection<BrandViewModel> Brands { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public string Rate { get; set; }
        public byte[] CarImage { get; set; }
        public string Image { get; set; }
    }
}
