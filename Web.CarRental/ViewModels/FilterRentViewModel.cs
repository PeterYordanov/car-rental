﻿using DAL.CarRental.CustomModel;
using System.Collections.Generic;

namespace Web.CarRental.ViewModels
{
    public class FilterRentViewModel
    {
        public virtual FilterRent FilterRent { get; set; }
        public virtual ICollection<RentViewModel> RentViewModel { get; set; }
    }
}
