﻿using DAL.CarRental.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.CarRental.ViewModels
{
    public class LocationViewModel
    {
        public int LocationId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}
