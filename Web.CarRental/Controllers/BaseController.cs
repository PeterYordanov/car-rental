﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Controllers
{
    public class BaseController : Controller
    {
        public void SetSignUpViewBag(string response)
        {
            ViewBag.SignUpError = true;
            ViewBag.SignUpMessage = response;
        }

        public void SetSignInViewBag()
        {
            ViewBag.SignInError = true;
            ViewBag.SignInPasswordMessage = "Wrong email or password.";
        }

        public void SetPasswordViewBag()
        {
            ViewBag.PasswordError = true;
            ViewBag.PasswordMessage = "Password do not match.";
        }
    }
}
