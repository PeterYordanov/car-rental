﻿using Microsoft.AspNetCore.Mvc;
using Web.CarRental.Services;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Controllers
{
    public class LocationController : BaseController
    {
        private readonly ILocationService<LocationViewModel> _locationService;

        public LocationController(ILocationService<LocationViewModel> locationService)
        {
            _locationService = locationService;
        }

        public IActionResult List()
        {
            var locations = _locationService.List();
            return View(locations);
        }

        public IActionResult Details(int id)
        {
            var location = _locationService.Get(id);
            return View(location);
        }

        public IActionResult Update(int id)
        {
            var location = _locationService.Get(id);
            return View(location);
        }

        public IActionResult Edit(LocationViewModel location)
        {
            _locationService.Update(location);
            return RedirectToAction(Constants.Route.List, Constants.Route.Location);
        }

        public IActionResult Create()
        {
            if (_locationService.CanCreate())
            {
                return View(new LocationViewModel { });
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(LocationViewModel location)
        {
            _locationService.Create(location);
            return RedirectToAction(Constants.Route.List, Constants.Route.Location);
        }

        public IActionResult Delete(int id)
        {
            if (_locationService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Location);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}