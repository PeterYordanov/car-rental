﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using Web.CarRental.Services;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Controllers
{
    public class CarController : BaseController
    {
        private readonly ICarService<CarViewModel> _carService;
        private readonly IBrandService<BrandViewModel> _brandService;
        private readonly ILocationService<LocationViewModel> _locationService;

        public CarController(ICarService<CarViewModel> carService, IBrandService<BrandViewModel> brandService, ILocationService<LocationViewModel> locationService)
        {
            _carService = carService;
            _brandService = brandService;
            _locationService = locationService;
        }

        public IActionResult List()
        {
            var cars = _carService.List();
            return View(cars);
        }

        public IActionResult Details(int id)
        {
            var car = _carService.Get(id);
            return View(car);
        }

        public IActionResult Update(int id)
        {
            var car = _carService.Get(id);
            car.Brands = _brandService.List();
            car.CurrentLocations = _locationService.List();
            return View(car);
        }

        public IActionResult Edit(CarViewModel car)
        {
            _carService.Update(car);
            return RedirectToAction(Constants.Route.List, Constants.Route.Car);
        }

        public IActionResult Create()
        {
            if (_carService.CanCreate())
            {
                var vm = new CarViewModel
                {
                    Brands = _brandService.List(),
                    CurrentLocations = _locationService.List()
                };
                return View(vm);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(CarViewModel car, BrandViewModel @class)
        {
            _carService.Create(car);
            return RedirectToAction(Constants.Route.List, Constants.Route.Car);
        }

        public IActionResult Delete(int id)
        {
            if (_carService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Car);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}