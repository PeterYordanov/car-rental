﻿using Microsoft.AspNetCore.Mvc;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.Services;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserService<UserViewModel> _userService;

        public UserController(IUserService<UserViewModel> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult SignUp()
        {
            var user = new UserViewModel();
            return View(user);
        }

        [HttpPost]
        public IActionResult SignUp(UserViewModel model)
        {
            if (!string.IsNullOrEmpty(_userService.SignUp(model)))
            {
                SetSignUpViewBag(_userService.SignUp(model));
                return View(model);
            }
            return RedirectToAction("SignIn", "User");
        }

        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SignIn(UserViewModel model)
        {
            if (_userService.SignIn(model).Role != null)
            {
                SetSignInViewBag();
            }
            return View("Profile", _userService.SignIn(model));
        }

        [HttpGet]
        public IActionResult SignOut()
        {
            _userService.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View(_userService.GetProfile());
        }

        public IActionResult Profile(UserViewModel user)
        {
            if (user.Password != user.ConfirmPassword)
            {
                SetPasswordViewBag();
                return View();
            }
            _userService.UpdateProfile(user);
            return RedirectToAction("SignIn", "User");
        }

        public IActionResult Settings(UserViewModel user)
        {
            _userService.Settings(user);
            return RedirectToAction("SignIn", "User");
        }


        public IActionResult List()
        {
            var users = _userService.List();
            return View(users);
        }

        public IActionResult Details(int id)
        {
            var user = _userService.Get(id);
            return View(user);
        }

        public IActionResult Update(int id)
        {
            var user = _userService.Get(id);
            return View(user);
        }

        public IActionResult Edit(UserViewModel user)
        {
            _userService.Update(user);
            return RedirectToAction(Constants.Route.List, Constants.Route.User);
        }

        public IActionResult Create()
        {
            if (_userService.CanCreate())
            {
                return View(new UserViewModel { });
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(UserViewModel user)
        {
            _userService.Create(user);
            return RedirectToAction(Constants.Route.List, Constants.Route.User);
        }

        public IActionResult Delete(int id)
        {
            if (_userService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.User);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}