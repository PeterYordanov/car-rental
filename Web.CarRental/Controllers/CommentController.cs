﻿using Microsoft.AspNetCore.Mvc;
using Web.CarRental.ViewModels;
using Web.CarRental.Services;
namespace Web.CarRental.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ICommentService<CommentViewModel> _commentService;
        private readonly IRentService<RentViewModel> _rentService;
        private readonly IUserService<UserViewModel> _userService;
        public CommentController(ICommentService<CommentViewModel> commentService, IRentService<RentViewModel> rentService, IUserService<UserViewModel> userService)
        {
            _commentService = commentService;
            _rentService = rentService;
            _userService = userService;
        }

        public IActionResult List()
        {
            var comments = _commentService.List();
            return View(comments);
        }

        public IActionResult Details(int id)
        {
            var comment = _commentService.Get(id);
            return View(comment);
        }

        public IActionResult Update(int id)
        {
            var comment = _commentService.Get(id);
            comment.Users = _userService.List();
            comment.Rents = _rentService.List();
            return View(comment);
        }

        public IActionResult Edit(CommentViewModel comment)
        {
            _commentService.Update(comment);
            return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
        }

        public IActionResult Create()
        {
            if (_commentService.CanCreate())
            {
                var vm = new CommentViewModel();
                vm.Users = _userService.List();
                vm.Rents = _rentService.List();
                return View(vm);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(CommentViewModel brand)
        {
            _commentService.Create(brand);
            return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
        }

        public IActionResult Delete(int id)
        {
            if (_commentService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Comment);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}