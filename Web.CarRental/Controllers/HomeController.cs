﻿using Microsoft.AspNetCore.Mvc;

namespace Web.CarRental.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
