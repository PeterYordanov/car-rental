﻿using DAL.CarRental.CustomModel;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Web.CarRental.Services;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Controllers
{
    public class RentController : BaseController
    {
        private readonly IRentService<RentViewModel> _rentService;
        private readonly ICarService<CarViewModel> _carService;
        private readonly IBrandService<BrandViewModel> _brandService;
        private readonly ILocationService<LocationViewModel> _pickupLocationService;
        private readonly ILocationService<LocationViewModel> _returnLocationService;

        public RentController(IRentService<RentViewModel> rentService,
            ICarService<CarViewModel> carService, IBrandService<BrandViewModel> brandService,
            ILocationService<LocationViewModel> pickupLocationService,
            ILocationService<LocationViewModel> returnLocationService)
        {
            _carService = carService;
            _rentService = rentService;
            _brandService = brandService;
            _pickupLocationService = pickupLocationService;
            _returnLocationService = returnLocationService;
        }


        public IActionResult List(FilterRentViewModel viewModel)
        {
            var model = new FilterRentViewModel();
            var rents = new List<RentViewModel>();
            if (viewModel.FilterRent != null)
            {
                if (viewModel.FilterRent.Country != null && viewModel.FilterRent.City != null && viewModel.FilterRent.Model != null && viewModel.FilterRent.Country != null)
                {
                    rents = _rentService.Search(viewModel);
                }
            }
            else
            {
                rents = _rentService.List();
               
            }
            model.RentViewModel = rents;
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var rent = _rentService.Get(id);
            return View(rent);
        }

        public IActionResult Update(int id)
        {
            var rent = _rentService.Get(id);
            rent.Cars = _carService.List();
            rent.PickupLocations = _pickupLocationService.List();
            rent.ReturnLocations = _returnLocationService.List();
            return View(rent);
        }

        public IActionResult Edit(RentViewModel rent)
        {
            _rentService.Update(rent);
            return RedirectToAction(Constants.Route.List, Constants.Route.Rent);
        }

        public IActionResult Book(RentViewModel rent)
        {
            rent.IsAvailable = false;
            _rentService.Update(rent);
            return RedirectToAction(Constants.Route.List, Constants.Route.Rent);
        }


        public IActionResult Create()
        {
            if (_rentService.CanCreate())
            {
                var vm = new RentViewModel();
                vm.Cars = _carService.List();
                vm.PickupLocations = _pickupLocationService.List();
                vm.ReturnLocations = _returnLocationService.List();
                return View(vm);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(RentViewModel rent)
        {
            _rentService.Create(rent);
            return RedirectToAction(Constants.Route.List, Constants.Route.Rent);
        }

        public IActionResult Delete(int id)
        {
            if (_rentService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Rent);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}