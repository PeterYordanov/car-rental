﻿using Microsoft.AspNetCore.Mvc;
using Web.CarRental.ViewModels;
using Web.CarRental.Services;
using System.Linq;
using System.Web.Mvc;

namespace Web.CarRental.Controllers
{
    public class BrandController : BaseController
    {
        private readonly IBrandService<BrandViewModel> _brandService;

        public BrandController(IBrandService<BrandViewModel> brandService)
        {
            _brandService = brandService;
        }

        public IActionResult List()
        {
            var brands = _brandService.List();
            return View(brands);
        }

        public IActionResult Details(int id)
        {
            var brand = _brandService.Get(id);
            return View(brand);
        }

        public IActionResult Update(int id)
        {
            var brand = _brandService.Get(id);
            return View(brand);
        }

        public IActionResult Edit(BrandViewModel brand)
        {
            _brandService.Update(brand);
            return RedirectToAction(Constants.Route.List, Constants.Route.Brand);
        }

        public IActionResult Create()
        {
            if (_brandService.CanCreate())
            {
                return View(new BrandViewModel { });
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }

        public IActionResult Insert(BrandViewModel brand)
        {
            _brandService.Create(brand);
            return RedirectToAction(Constants.Route.List, Constants.Route.Brand);
        }

        public IActionResult Delete(int id)
        {
            if (_brandService.IsDeleted(id))
            {
                return RedirectToAction(Constants.Route.List, Constants.Route.Brand);
            }
            return RedirectToAction(Constants.Route.SignIn, Constants.Route.User);
        }
    }
}