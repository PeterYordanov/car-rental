﻿using System.Collections.Generic;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public class BrandService<T> : GenericService, IBrandService<T>
    {
        private readonly IHttpRequestService<BrandViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public BrandService(IHttpRequestService<BrandViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<BrandViewModel> List()
        {
            List<BrandViewModel> brands = new List<BrandViewModel>();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                brands = _httpService.List(Constants.API.Brands, _sessionManager.Token);
            }
            return brands;
        }

        public BrandViewModel Get(int id)
        {
            BrandViewModel brand = new BrandViewModel();

            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                brand = _httpService.Get(Constants.API.Brands, id, _sessionManager.Token);
            }
            return brand;
        }

        public BrandViewModel Update(BrandViewModel model)
        {
            BrandViewModel brand = new BrandViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                brand = _httpService.Update(Constants.API.Brands, model, _sessionManager.Token);
            }
            return brand;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public BrandViewModel Create(BrandViewModel model)
        {
            BrandViewModel brand = new BrandViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                brand = _httpService.Create(Constants.API.Brands, model, _sessionManager.Token);
            }
            return brand;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.API.Brands, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
