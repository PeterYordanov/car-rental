﻿using System.Collections.Generic;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public interface ICarService<T>
    {
        List<CarViewModel> List();
        CarViewModel Get(int id);
        CarViewModel Update(CarViewModel model);
        CarViewModel Create(CarViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
