﻿using System;
using System.Collections.Generic;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public class CarService<T> : GenericService, ICarService<T>
    {
        private readonly IHttpRequestService<CarViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public CarService(IHttpRequestService<CarViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<CarViewModel> List()
        {
            List<CarViewModel> cars = new List<CarViewModel>();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                cars = _httpService.List(Constants.API.Cars, _sessionManager.Token);
                foreach (var car in cars)
                {
                    if (car.CarImage != null)
                    {
                        var base64 = Convert.ToBase64String(car.CarImage);
                        car.Image = string.Format("data:image/gif;base64,{0}", base64);
                    }
                }
            }
            return cars;
        }

        public CarViewModel Get(int id)
        {
            CarViewModel car = new CarViewModel();

            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                car = _httpService.Get(Constants.API.Cars, id, _sessionManager.Token);
                if (car.CarImage != null)
                {
                    var base64 = Convert.ToBase64String(car.CarImage);
                    car.Image = string.Format("data:image/gif;base64,{0}", base64);
                }
            }
            return car;
        }

        public CarViewModel Update(CarViewModel model)
        {
            CarViewModel car = new CarViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                car = _httpService.Update(Constants.API.Cars, model, _sessionManager.Token);
            }
            return car;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public CarViewModel Create(CarViewModel model)
        {
            CarViewModel car = new CarViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                car = _httpService.Create(Constants.API.Cars, model, _sessionManager.Token);
            }
            return car;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.API.Cars, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
