﻿using System.Collections.Generic;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public interface ILocationService<T>
    {
        List<LocationViewModel> List();
        LocationViewModel Get(int id);
        LocationViewModel Update(LocationViewModel model);
        LocationViewModel Create(LocationViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
