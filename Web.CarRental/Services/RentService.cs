﻿using DAL.CarRental.CustomModel;
using System;
using System.Collections.Generic;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public class RentService<T> : GenericService, IRentService<T>
    {
        private readonly IHttpRequestService<RentViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public RentService(IHttpRequestService<RentViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<RentViewModel> Search(FilterRentViewModel model)
        {
            List<RentViewModel> filtered = new List<RentViewModel>();
            filtered = _httpService.Search(Constants.API.Search, model) ?? new List<RentViewModel>();
            foreach (var car in filtered)
            {
                if (car.CarImage != null)
                {
                    var base64 = Convert.ToBase64String(car.CarImage);
                    car.Image = string.Format("data:image/gif;base64,{0}", base64);
                }
            }
            return filtered;
        }

        public List<RentViewModel> List()
        {
            List<RentViewModel> rents = new List<RentViewModel>();
            rents = _httpService.List(Constants.API.Rents, _sessionManager.Token) ?? new List<RentViewModel>();
            foreach (var car in rents)
            {
                if (car.CarImage != null)
                {
                    var base64 = Convert.ToBase64String(car.CarImage);
                    car.Image = string.Format("data:image/gif;base64,{0}", base64);
                }
            }

            return rents;
        }

        public RentViewModel Get(int id)
        {
            RentViewModel rent = new RentViewModel();
            rent = _httpService.Get(Constants.API.Rents, id, _sessionManager.Token) ?? new RentViewModel();
            if (rent.CarImage != null)
            {
                var base64 = Convert.ToBase64String(rent.CarImage);
                rent.Image = string.Format("data:image/gif;base64,{0}", base64);
            }

            return rent;
        }

        public RentViewModel Update(RentViewModel model)
        {
            RentViewModel rent = new RentViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                rent = _httpService.Update(Constants.API.Rents, model, _sessionManager.Token);
            }
            return rent;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public RentViewModel Create(RentViewModel model)
        {
            RentViewModel rent = new RentViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                rent = _httpService.Create(Constants.API.Rents, model, _sessionManager.Token);
            }
            return rent;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.API.Rents, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
