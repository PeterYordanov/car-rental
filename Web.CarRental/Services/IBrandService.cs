﻿using System.Collections.Generic;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public interface IBrandService<T> 
    {
        List<BrandViewModel> List();
        BrandViewModel Get(int id);
        BrandViewModel Update(BrandViewModel model);
        BrandViewModel Create(BrandViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}