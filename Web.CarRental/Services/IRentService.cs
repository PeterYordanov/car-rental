﻿using DAL.CarRental.CustomModel;
using System.Collections.Generic;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public interface IRentService<T>
    {
        List<RentViewModel> Search(FilterRentViewModel model);
        List<RentViewModel> List();
        RentViewModel Get(int id);
        RentViewModel Update(RentViewModel model);
        RentViewModel Create(RentViewModel model);
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
