﻿using System.Collections.Generic;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.ViewModels;

namespace Web.CarRental.Services
{
    public class LocationService<T> : GenericService, ILocationService<T>
    {
        private readonly IHttpRequestService<LocationViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public LocationService(IHttpRequestService<LocationViewModel> httpService, SessionManager sessionManager) : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public List<LocationViewModel> List()
        {
            List<LocationViewModel> locations = new List<LocationViewModel>();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                locations = _httpService.List(Constants.API.Locations, _sessionManager.Token);
            }
            return locations;
        }

        public LocationViewModel Get(int id)
        {
            LocationViewModel location = new LocationViewModel();

            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                location = _httpService.Get(Constants.API.Locations, id, _sessionManager.Token);
            }
            return location;
        }

        public LocationViewModel Update(LocationViewModel model)
        {
            LocationViewModel location = new LocationViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                location = _httpService.Update(Constants.API.Locations, model, _sessionManager.Token);
            }
            return location;
        }

        public bool CanCreate()
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = true;
            }
            return result;
        }

        public LocationViewModel Create(LocationViewModel model)
        {
            LocationViewModel location = new LocationViewModel();
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                location = _httpService.Create(Constants.API.Locations, model, _sessionManager.Token);
            }
            return location;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin || _sessionManager.IsBackOffice)
            {
                result = _httpService.Delete(Constants.API.Locations, id, _sessionManager.Token);
            }
            return result;
        }
    }
}
