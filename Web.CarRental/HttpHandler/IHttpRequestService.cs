﻿using DAL.CarRental.CustomModel;
using System.Collections.Generic;
using Web.CarRental.ViewModels;

namespace Web.CarRental.HttpHandler
{
    public interface IHttpRequestService<T> where T : class
    {
        List<T> List(string str, string token);
        List<T> Search(string str, FilterRentViewModel model);
        T Get(string str, int id, string token);
        T Create(string str, T model, string token);
        T Update(string str, T model, string token);
        UserViewModel GetProfile(string str, int id, string token);
        UserViewModel SignIn(UserViewModel user);
        string SignUp(T user);
        bool Delete(string str, int id, string token);
    }
}
