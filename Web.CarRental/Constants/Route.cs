﻿namespace Web.CarRental.Constants
{
    public class Route
    {
        public const string SignIn = "SignIn";
        public const string SignUp = "SignUp";
        public const string Update = "Update";
        public const string List = "List";
        public const string Details = "Details";
        public const string Home = "Home";
        public const string Location = "Location";
        public const string Brand = "Brand";
        public const string Car = "Car";
        public const string User = "User";
        public const string Rent = "Rent";
        public const string Comment = "Comment";
    }
}
