﻿using System.Collections.Generic;

namespace Web.CarRental.Constants
{
    public static class API
    {
        public const string Url = "https://localhost:44323/api/";
        public const string SignUp = "auth/signup";
        public const string SignIn = "auth/signin";
        public const string Users = "users";
        public const string Rents = "rents";
        public const string Search = "rents/search";
        public const string Cars = "cars";
        public const string Brands = "brands";
        public const string Comments = "comments";
        public const string Locations = "locations";
        public const string HeaderName = "Accept";
        public const string HeaderValue = "application/json";
        public const string Bearer = "Bearer";
    }
}
