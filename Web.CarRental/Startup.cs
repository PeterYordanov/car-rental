﻿using AutoMapper;
using DAL.CarRental.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Web.CarRental.AuthHandler;
using Web.CarRental.HttpHandler;
using Web.CarRental.Services;
using Web.CarRental.ViewModels;

namespace Web.CarRental
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddScoped<IHttpRequestService<UserViewModel>, HttpRequestService<UserViewModel>>();
            services.AddScoped<IHttpRequestService<RentViewModel>, HttpRequestService<RentViewModel>>();
            services.AddScoped<IHttpRequestService<CarViewModel>, HttpRequestService<CarViewModel>>();
            services.AddScoped<IHttpRequestService<BrandViewModel>, HttpRequestService<BrandViewModel>>();
            services.AddScoped<IHttpRequestService<LocationViewModel>, HttpRequestService<LocationViewModel>>();
            services.AddScoped<IHttpRequestService<CommentViewModel>, HttpRequestService<CommentViewModel>>();

            services.AddScoped<IBrandService<BrandViewModel>, BrandService<BrandViewModel>>();
            services.AddScoped<ILocationService<LocationViewModel>, LocationService<LocationViewModel>>();
            services.AddScoped<ICarService<CarViewModel>, CarService<CarViewModel>>();
            services.AddScoped<IRentService<RentViewModel>, RentService<RentViewModel>>();
            services.AddScoped<IUserService<UserViewModel>, UserService<UserViewModel>>();
            services.AddScoped<ICommentService<CommentViewModel>, CommentService<CommentViewModel>>();

            services.AddSingleton<SessionManager>();
            services.AddDistributedMemoryCache();
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSession();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
