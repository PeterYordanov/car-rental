﻿using DAL.CarRental.Models;
using API.CarRental;
using DAL.CarRental.Repositories;
using API.CarRental.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DAL.CarRental;

namespace API.CarRental.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.AdminBackOffice)]
    public class BrandsController : ControllerBase
    {
        private readonly IRepository<Brand> _repo;

        public BrandsController(CarContext context)
        {
            _repo = new Repository<Brand>(context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var brands = _repo.GetAll();
            if (Condition.ValidateObjects(brands))
            {
                return NotFound();
            }
            return Ok(brands);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var brand = _repo.GetById(id);
            if (Condition.ValidateObject(brand))
            {
                return NotFound();
            }
            return Ok(brand);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Brand brand)
        {
            _repo.Insert(brand);
            if (Condition.ValidateObject(brand))
            {
                return NotFound();
            }
            return Created(Message.CreatedBrand, brand);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Brand brand)
        {
            _repo.Edit(brand);
            return Ok(brand);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var brand = _repo.GetById(id);
            if (Condition.ValidateObject(brand))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(brand);
                return Ok();
            }
        }
    }
}
