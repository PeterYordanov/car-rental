﻿using API.CarRental.Extensions;
using DAL.CarRental.Models;
using DAL.CarRental;
using DAL.CarRental.Repositories;
using Microsoft.AspNetCore.Mvc;
using DAL.CarRental.CustomModel;

namespace API.CarRental.Controllers
{
    [Route("api/[controller]")]
    public class RentsController : ControllerBase
    {
        private readonly IRentRepository<Rent> _rentRepo;
        private readonly IRepository<Rent> _repo;

        public RentsController(CarContext context)
        {
			_rentRepo = new RentRepository<Rent>(context);
            _repo = new Repository<Rent>(context);
        }

        [HttpGet("search")]
        public ObjectResult Search([FromQuery] FilterRent request)
        {
            var rents = _rentRepo.SearchRent(request.Country,
                                         request.City,
                                         request.Model,
                                         request.Amount);
            return Ok(rents);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var rents = _rentRepo.GetRents();
            if (Condition.ValidateObjects(rents))
            {
                return NotFound();
            }
            return Ok(rents);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var rent = _rentRepo.GetRentById(id);
            if (Condition.ValidateObject(rent))
            {
                return NotFound();
            }
            return Ok(rent);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Rent rent)
        {
            _repo.Insert(rent);
            if (Condition.ValidateObject(rent))
            {
                return NotFound();
            }
            return Created(Message.CreatedRent, rent);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Rent rent)
        {
            _repo.Edit(rent);
            return Ok(rent);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var rent = _repo.GetById(id);
            if (Condition.ValidateObject(rent))
            {
                return NotFound();
            }
            else
            {
                _rentRepo.Delete(rent);
                return Ok();
            }
        }
    }
}
