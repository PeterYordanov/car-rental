﻿using API.CarRental.Extensions;
using DAL.CarRental;
using DAL.CarRental.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DAL.CarRental;
using DAL.CarRental.Models;

namespace API.CarRental.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.AdminBackOffice)]
    public class LocationsController : ControllerBase
    {
        private readonly IRepository<Location> _repo;

        public LocationsController(CarContext context)
        {
            _repo = new Repository<Location>(context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var locations = _repo.GetAll();
            if (Condition.ValidateObjects(locations))
            {
                return NotFound();
            }
            return Ok(locations);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var location = _repo.GetById(id);
            if (Condition.ValidateObject(location))
            {
                return NotFound();
            }
            return Ok(location);
        }

        [HttpPost]
        public IActionResult Post([FromBody] Location location)
        {
            _repo.Insert(location);
            if (Condition.ValidateObject(location))
            {
                return NotFound();
            }
            return Created(Message.CreatedLocation, location);
        }

        [HttpPut]
        public IActionResult Put([FromBody] Location location)
        {
            _repo.Edit(location);
            return Ok(location);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var location = _repo.GetById(id);
            if (Condition.ValidateObject(location))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(location);
                return Ok();
            }
        }
    }
}
