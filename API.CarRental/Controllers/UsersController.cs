﻿using API.CarRental.Extensions;
using DAL.CarRental.Models;
using DAL.CarRental;
using DAL.CarRental.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.CarRental.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repo;

        public UsersController(CarContext context)
        {
            _repo = new Repository<User>(context);
        }

        [HttpGet]
        [Authorize(Roles = Role.Admin)]
        public IActionResult Get()
        {
            var users = _repo.GetAll();
            if (Condition.ValidateObjects(users))
            {
                return NotFound();
            }
            return Ok(users);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public IActionResult Get(int id)
        {
            var user = _repo.GetById(id);
            if (Condition.ValidateObject(user))
            {
                return NotFound();
            }
            return Ok(user);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
			user.Firstname = user.Firstname.FirstLetterToUpperCase();
			user.Lastname = user.Lastname.FirstLetterToUpperCase();
			user.Email = user.Email.ToLower();
			user.Role = user.Role.FirstLetterToUpperCase();
			user.Password = StringExtension.SHA512(user.Password);
            _repo.Insert(user);
            if(Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            return Created(Message.CreatedUser, user);
        }

        [HttpPut]
        public IActionResult Put([FromBody] User user)
        {
            if (Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            user.Password = StringExtension.SHA512(user.Password);
            _repo.Edit(user);   
            return Ok(user);
        }

        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _repo.GetById(id);
            if (Condition.ValidateObject(user))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(user);
                return Ok();
            }
        }
    }
}
