﻿using DAL.CarRental.Models;
using API.CarRental.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DAL.CarRental;
using DAL.CarRental.Repositories;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.IO;
using System;

namespace API.CarRental.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = Role.AdminBackOffice)]
    public class CarsController : ControllerBase
    {
        private readonly ICarRepository<Car> _carRepo;
        private readonly IRepository<Car> _repo;

        public CarsController(CarContext context)
        {
            _carRepo = new CarRepository<Car>(context);
            _repo = new Repository<Car>(context);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var cars = _carRepo.GetCars();
            if (Condition.ValidateObjects(cars))
            {
                return NotFound();
            }
            return Ok(cars);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var car = _carRepo.GetCarById(id);
            if (Condition.ValidateObject(car))
            {
                return NotFound();
            }
            return Ok(car);
        }

        [HttpPost]
        public IActionResult Post(IFormFile carImage, int locationId, int brandId, int amount)
        {
            var car = new Car
            {
                CurrentLocationId = locationId,
                BrandId = brandId,
                Amount = amount,
            };

            if (carImage.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    carImage.CopyTo(ms);
                    car.CarImage = ms.ToArray();
                }
            }

            _repo.Insert(car);
            if (Condition.ValidateObject(car))
            {
                return NotFound();
            }
            return Created(Message.CreatedCar, car);
        }

        [HttpPut]

        public IActionResult Put(IFormFile carImage, int carId, int locationId, int brandId, int amount)
        {
            var car = _carRepo.GetCarById(carId);
            car = new Car
            {
                CarId = carId,
                CurrentLocationId = locationId,
                BrandId = brandId,
                Amount = amount,
            };

            if (carImage.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    carImage.CopyTo(ms);
                    car.CarImage = ms.ToArray();
                }
            }

            _repo.Edit(car);
            return Ok(car);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var car = _repo.GetById(id);
            if (Condition.ValidateObject(car))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(car);
                return Ok();
            }
        }
    }
}
