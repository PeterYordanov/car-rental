﻿namespace API.CarRental.Extensions
{
    public static class Message
    {
        public const string Conflict = "User with this email already exists.";
        public const string BadRequestUser = "Firstname, lastname, email, password, role are required fields.";
        public const string BadRequestPassword = "Passwords are empty or do not match.";
        public const string Unauthorized = "Wrong email or password.";
        public const string CreatedUser = "Successfully added new user.";
        public const string CreatedBrand = "Successfully added new brand.";
        public const string CreatedComment = "Successfully added new comment.";
        public const string CreatedCar = "Successfully added new car.";
        public const string CreatedRent = "Successfully added new rent.";
        public const string CreatedLocation = "Successfully added new location.";
    }
}
