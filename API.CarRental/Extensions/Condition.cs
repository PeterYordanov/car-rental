﻿using DAL.CarRental.Models;
using System.Collections.Generic;

namespace API.CarRental.Extensions
{
    public static class Condition
    {
        public static bool ValidateUser(User user, string firstname, string lastname, string email, string password, string role)
        {
            if (user == null || string.IsNullOrEmpty(firstname) ||
                string.IsNullOrEmpty(lastname) || string.IsNullOrEmpty(email) ||
                string.IsNullOrEmpty(password) || string.IsNullOrEmpty(role))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidatePassword(string password, string confirmPassword)
        {
            if ((string.IsNullOrEmpty(password) && string.IsNullOrEmpty(confirmPassword)) &&
                password != confirmPassword)

            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool ValidateObjects(List<User> users)
        {
            if (users == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Comment> comments)
        {
            if (comments == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Brand> brands)
        {
            if (brands == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Car> cars)
        {
            if (cars == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Location> locations)
        {
            if (locations == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObjects(List<Rent> rents)
        {
            if (rents == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(User user)
        {
            if (user == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ValidateObject(Comment comment)
        {
            if (comment == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(Brand brand)
        {
            if (brand == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(Car car)
        {
            if (car == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(Location location)
        {
            if (location == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ValidateObject(Rent rent)
        {
            if (rent == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
